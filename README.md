# OpenCV_notebooks

##  Some useful OpenCV functions for image_processing

## 1. Histogram Of Oriented Gradients 

## 2. ORB Algorithm (Oriented FAST and Rotated BRIEF)
 
  *FAST* ORB is basically a fusion of FAST keypoint detector and BRIEF descriptor with many modifications to enhance the performance. 
  First it use FAST to find keypoints, then apply Harris corner measure to find top N points among them. It also use pyramid to produce multiscale-features. 
  But one problem is that, FAST doesn’t compute the orientation.
  
  *BRIEF* BRIEF comes into picture at this moment. It provides a shortcut to find the binary strings directly without finding descriptors. 
  It takes smoothened image patch and selects a set of n_d
  (x,y) location pairs in an unique way (explained in paper). Then some pixel intensity comparisons are done on these location pairs. 
  For eg, let first location pairs be p and q. If I(p) < I(q), then its result is 1, else it is 0. This is applied for all the n_d 
  location pairs to get a n_d-dimensional bitstring.
  
  BRIEF is a feature descriptor, it doesn’t provide any method to find the features. 
  
## 3. Fourier Transforms of images